# Mess food management application

The main aim of this application is to reduce the wastage of food in hostel mess. Due to some reasons students are not having their lunch or breakfast or dinner, mess food management makes students to notify whether they want lunch or breakfast or dinner today at the time one hour before lunch. So that this application make an http request to the mess manager so that amount of food prepared will be sufficient to the current number of students.
This application makes use of a centralized database server and each http request from the application can be maintained by server.
Here I have used python flask for handling my http request and pymongo - a mongodb database for storing the student and mess incharge details. 
Here the security will be maintained in *encrypting the password* using **bcrypt** .
