from flask import Flask
from flask import jsonify
from flask import request
from flask_pymongo import PyMongo
import datetime
import bcrypt
import json
import time
from bson.json_util import dumps

app = Flask(__name__)

app.config['MONGO_DBNAME'] = 'messfood'
app.config['MONGO_URI'] = 'mongodb://localhost:27017/messfood'


mongo = PyMongo(app)

#@app.route('/star', methods=['GET'])
#def get_all_stars():
  #star = mongo.db.stars
  #output = []
  #for s in star.find():
    #output.append({'name' : s['name'], 'distance' : s['distance']})
  #return jsonify({'result' : output})

@app.route('/stureg', methods=['GET','POST'])
def save_student():
  table = mongo.db.studentsignup

  content = request.get_json()
  
  passwdd = str(content['password'])

  hashed = bcrypt.hashpw(passwdd , bcrypt.gensalt())
  
  myrecord2 = [
        { "rollno(hostelid)" : content['rollno(hostelid)'],
          "name" : content['name'],
          "department" : content['department'],
          "password" :hashed,
          "year" : content['year'],
          "mess" : content['mess'],
          "hostelname" : content['hostelname'],
          "registereddate" : datetime.datetime.today() 
           },

          
        ]

  table.insert(myrecord2)
  return jsonify({'result' : 'sucessfully inserted'})

@app.route('/stulog', methods=['GET','POST'])
def login_student():
  table = mongo.db.studentsignup

  content = request.get_json()

  hpass = str(content['password'])
   
  newhid = table.find_one({'rollno(hostelid)':content['rollno(hostelid)']})

  newhpass = {'password':newhid['password']}

  x=json.dumps(newhpass)


  y=json.loads(x)

  

  if  bcrypt.checkpw(hpass,str(y['password'])):
  	return jsonify({'result' : 'sucessfully logged in'})

  else:
 	exit()

@app.route('/stufood', methods=['GET','POST'])
def save_session():
  table = mongo.db.studentfood

  content = request.get_json()

  newtab = table.find_one({'rollno(hostelid)':content['rollno(hostelid)']})

  


  if newtab:
   
    uphid = table.update({'rollno(hostelid)':content['rollno(hostelid)']},{'$set':{'session':content['session'],'acceptance':content['acceptance'],'fixeddate':datetime.date.today().strftime('%d-%m-%Y'), 
          "fixedtime" : datetime.datetime.now().strftime('%H:%M:%S'),}});
    return jsonify({'result' : 'sucessfully updated'})

  else:
 
     myrecord2 = [
        { "rollno(hostelid)" : content['rollno(hostelid)'],
          "session":content['session'],
          "acceptance":content['acceptance'],
          "fixeddate":datetime.date.today().strftime('%d-%m-%Y'), 
          "fixedtime" : datetime.datetime.now().strftime('%H:%M:%S')
         
          },

       ]
     table.insert(myrecord2)  
     
     return jsonify({'result' : 'sucessfully inserted'}) 
  
  

 

@app.route('/admlog', methods=['GET','POST'])
def login_admin():
  table = mongo.db.adminsignup

  content = request.get_json()

  hpass = str(content['password'])
   
  newhid = table.find_one({'supervisiorid':content['supervisiorid']})

  newhpass = {'password':newhid['password']}

  x=json.dumps(newhpass)


  y=json.loads(x)

  

  if  bcrypt.checkpw(hpass,str(y['password'])):
  	return jsonify({'result' : 'sucessfully logged in'})

  else:
 	exit()
@app.route('/stulist', methods=['GET','POST'])
def list_stu():
  

  table = mongo.db.studentfood

  new_idd = table.find({'acceptance':"No" },{'rollno(hostelid)':2})
  
  newi = dumps(new_idd)
  
  return jsonify({'result':newi})

@app.route('/stulistcount', methods=['GET','POST'])
def count_stu():
  
  table = mongo.db.studentfood 

  count_idd = table.find({'acceptance':"No" },{'rollno(hostelid)':2}).count()

  newii = dumps(count_idd)
   
  return jsonify({'result':newii})




  
  

if __name__ == '__main__':
    app.run(host='192.168.43.238',debug=True)

