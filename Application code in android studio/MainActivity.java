package com.example.messfoodapp;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

import android.app.ProgressDialog;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity {

    private TextView register;
    private EditText hid;
    private EditText name;
    private EditText department;
    private EditText password;
    private EditText year;
    private EditText mess;
    private EditText hostelname;

    private Button stureg;

    private ProgressDialog progressDialog;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        register=(TextView)findViewById(R.id.reg);
        hid=(EditText)findViewById(R.id.hid);
        name=(EditText)findViewById(R.id.name);
        password=(EditText)findViewById(R.id.password);
        hostelname=(EditText)findViewById(R.id.hname);
        mess=(EditText)findViewById(R.id.mess);
        year=(EditText)findViewById(R.id.year);
        department=(EditText)findViewById(R.id.dept);

        stureg=(Button)findViewById(R.id.studentsignup);


        stureg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                progressDialog = new ProgressDialog(MainActivity.this);
                progressDialog.setMessage("Registering User...");
                progressDialog.show();
                new HTTPAsyncTask().execute();



            }
        });



    }

    private class HTTPAsyncTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {
            // params comes from the execute() call: params[0] is the url.
            try {
                try {
                    return HttpPost("http://192.168.43.238:5000/stureg");
                } catch (JSONException e) {
                    e.printStackTrace();
                    return "Error!";
                }
            } catch (IOException e) {
                return "Unable to retrieve web page. URL may be invalid.";
            }
        }

        @Override
        protected void onPostExecute(String result) {

            register.setText(result);

            progressDialog.dismiss();

            String regs = register.getText().toString();

            if(regs.equals("OK"))
            {
                startActivity(new Intent(MainActivity.this, Login2Activity.class));
            }


        }

    }

    private String HttpPost(String myUrl) throws IOException, JSONException {
        String result = "";

        URL url = new URL(myUrl);

        // 1. create HttpURLConnection
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("POST");
        conn.setRequestProperty("Content-Type", "application/json; charset=utf-8");

        // 2. build JSON object
        JSONObject jsonObject = buidJsonObject();

        // 3. add JSON content to POST request body
        setPostRequestContent(conn, jsonObject);

        // 4. make POST request to the given URL
        conn.connect();


        // 5. return response message
        return conn.getResponseMessage()+"";

    }



    private JSONObject buidJsonObject() throws JSONException {

        JSONObject jsonObject = new JSONObject();
        jsonObject.accumulate("rollno(hostelid)", hid.getText().toString());
        jsonObject.accumulate("name",  name.getText().toString());
        jsonObject.accumulate("password",  password.getText().toString());
        jsonObject.accumulate("hostelname", hostelname.getText().toString());
        jsonObject.accumulate("mess",  mess.getText().toString());
        jsonObject.accumulate("year",  year.getText().toString());
        jsonObject.accumulate("department",  department.getText().toString());

        return jsonObject;
    }

    private void setPostRequestContent(HttpURLConnection conn,
                                       JSONObject jsonObject) throws IOException {

        OutputStream os = conn.getOutputStream();
        BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
        writer.write(jsonObject.toString());
        Log.i(MainActivity.class.toString(), jsonObject.toString());
        writer.flush();
        writer.close();
        os.close();
    }



}
