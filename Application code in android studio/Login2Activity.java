package com.example.messfoodapp;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

public class Login2Activity extends AppCompatActivity {


    private EditText hidd;
    private TextView resg;
    private EditText passwordd;
    private Button logsin;
    private Button signup;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login2);

        hidd=(EditText)findViewById(R.id.hidd);
        resg=(TextView)findViewById(R.id.regggs);
        passwordd=(EditText)findViewById(R.id.passwordd);

        logsin=(Button)findViewById(R.id.loginss);
        signup=(Button)findViewById(R.id.register);

        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Login2Activity.this, MainActivity.class));
            }
        });
        logsin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressDialog = new ProgressDialog(Login2Activity.this);
                progressDialog.setMessage("Logging In...");
                progressDialog.show();
                new Login2Activity.weatherdata().execute();

            }
        });


    }
    private class weatherdata extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {
            // params comes from the execute() call: params[0] is the url.
            try {
                try {
                    return HttpPost("http://192.168.43.238:5000/stulog");
                } catch (JSONException e) {
                    e.printStackTrace();
                    return "Error!";
                }
            } catch (IOException e) {
                return "invalid password";
            }
        }

        @Override
        protected void onPostExecute(String result) {

            resg.setText(result);

            progressDialog.dismiss();

            String sp=resg.getText().toString();

            if(sp.equals("OK"))
            {
                Intent in=new Intent(Login2Activity.this, Session2Activity.class);
                in.putExtra("hid",hidd.getText().toString());
                startActivity(in);

            }



        }

    }

    private String HttpPost(String myUrl) throws IOException, JSONException {
        String result = "";

        URL url = new URL(myUrl);

        // 1. create HttpURLConnection
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("POST");
        conn.setRequestProperty("Content-Type", "application/json; charset=utf-8");

        // 2. build JSON object
        JSONObject jsonObject = buidJsonObject();

        // 3. add JSON content to POST request body
        setPostRequestContent(conn, jsonObject);

        // 4. make POST request to the given URL
        conn.connect();


        // 5. return response message
        return conn.getResponseMessage()+"";

    }



    private JSONObject buidJsonObject() throws JSONException {

        JSONObject jsonObject = new JSONObject();
        jsonObject.accumulate("rollno(hostelid)", hidd.getText().toString());
        jsonObject.accumulate("password",  passwordd.getText().toString());


        return jsonObject;
    }

    private void setPostRequestContent(HttpURLConnection conn,
                                       JSONObject jsonObject) throws IOException {

        OutputStream os = conn.getOutputStream();
        BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
        writer.write(jsonObject.toString());
        Log.i(MainActivity.class.toString(), jsonObject.toString());
        writer.flush();
        writer.close();
        os.close();
    }






}
