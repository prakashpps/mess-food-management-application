package com.example.messfoodapp;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

public class Session2Activity extends AppCompatActivity {

    private EditText hidd;
    private TextView sess;
    private Button sessionn;
    private Spinner spinner1,spinner2;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_session2);

        hidd=(EditText)findViewById(R.id.hidd);
        sess=(TextView)findViewById(R.id.sess);
        sessionn=(Button)findViewById(R.id.ssesion);
        spinner1=(Spinner)findViewById(R.id.spinner1);
        spinner2=(Spinner)findViewById(R.id.spinner2);

        String hses=getIntent().getStringExtra("hid");

        hidd.setText(hses);

        sessionn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressDialog = new ProgressDialog(Session2Activity.this);
                progressDialog.setMessage("Updating data...");
                progressDialog.show();
                new Session2Activity.savedata().execute();

            }
        });
    }

    private class savedata extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {
            // params comes from the execute() call: params[0] is the url.
            try {
                try {
                    return HttpPost("http://192.168.43.238:5000/stufood");
                } catch (JSONException e) {
                    e.printStackTrace();
                    return "Error!";
                }
            } catch (IOException e) {
                return "URL may be invalid.";
            }
        }

        @Override
        protected void onPostExecute(String result) {

            sess.setText(result);

            progressDialog.dismiss();

        }

    }

    private String HttpPost(String myUrl) throws IOException, JSONException {
        String result = "";

        URL url = new URL(myUrl);

        // 1. create HttpURLConnection
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("POST");
        conn.setRequestProperty("Content-Type", "application/json; charset=utf-8");

        // 2. build JSON object
        JSONObject jsonObject = buidJsonObject();

        // 3. add JSON content to POST request body
        setPostRequestContent(conn, jsonObject);

        // 4. make POST request to the given URL
        conn.connect();


        // 5. return response message
        return conn.getResponseMessage()+"";

    }



    private JSONObject buidJsonObject() throws JSONException {

        JSONObject jsonObject = new JSONObject();
        jsonObject.accumulate("rollno(hostelid)", hidd.getText().toString());
        jsonObject.accumulate("session",  String.valueOf(spinner1.getSelectedItem()));
        jsonObject.accumulate("acceptance", String.valueOf(spinner2.getSelectedItem()));


        return jsonObject;
    }

    private void setPostRequestContent(HttpURLConnection conn,
                                       JSONObject jsonObject) throws IOException {

        OutputStream os = conn.getOutputStream();
        BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
        writer.write(jsonObject.toString());
        Log.i(MainActivity.class.toString(), jsonObject.toString());
        writer.flush();
        writer.close();
        os.close();
    }



}
